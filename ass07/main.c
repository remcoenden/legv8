/* main.c simple program to multiply two values */

#include <stdio.h>

#define TEST_SIZE 8

extern unsigned long long int power(unsigned long long int n, unsigned long long int m);

int main(void)
{
	unsigned long long int a[TEST_SIZE] = {0, 1, 0, 1, 2, 32, 21, 7};
	unsigned long long int b[TEST_SIZE] = {0, 0, 1, 1, 2, 2,  7, 21};
	unsigned long long int result;

	int i;
	for(i = 0; i < TEST_SIZE; i++) {
		result = power(a[i], b[i]);
		printf("Result of power(%llu, %llu) = %llu\n", a[i], b[i], result);
	}

    while(1);
}
