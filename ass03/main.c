/* main.c simple program to multiply two values */

#include <stdio.h>

#define TEST_SIZE 9

extern unsigned long long int multiply(unsigned long long int a, unsigned long long int b);

int main(void)
{
	unsigned long long int a[TEST_SIZE] = {0, 1, 0, 1, 2000, 2, 1000000, 1, 4294967295};
	unsigned long long int b[TEST_SIZE] = {0, 0, 1, 1, 2, 4294967295, 4294967295, 18446744073709551615u, 4294967296};
	long long int result;

	int i;
	for(i = 0; i < TEST_SIZE; i++) {
		result = multiply(a[i], b[i]);
		printf("Result of multiply(%llu, %llu) = %llu\n", a[i], b[i], result);
	}

    while(1);
}
